//
//  ItemRoutes.swift
//  App
//
//  Created by Imthath M on 19/05/19.
//

import Vapor

struct ItemRoutes: RouteCollection {
    
    func boot(router: Router) throws {
        router.get(Path.categoryItems, use: listCategoryItems)
        router.get(Path.shopItems, use: listShopItems)
        router.get(Path.orderItems, use: listOrderItems)
        router.post(Path.changeItemAvailability, use: changeAvailability)
    }
    
    func listCategoryItems(_ request: Request) throws -> Future<[Item]> {
        return try request.parameters.next(Category.self).flatMap { category in
            return try category.items.query(on: request).all()  
        }
    }
    
    func listShopItems(_ request: Request) throws -> Future<[Item]> {
        return try request.parameters.next(Shop.self).flatMap { shop in
            return try shop.items.query(on: request).all()
        }
    }
    
    func listOrderItems(_ request: Request) throws -> Future<[Item.Info]> {
        return try request.parameters.next(Order.self).flatMap { order in
            return try order.orderItems.query(on: request).all().map { items in
                return items.uniqueCount()
            }
        }
    }
    
    func changeAvailability(_ request: Request) throws -> Future<Item> {
        return try request.parameters.next(Item.self).flatMap { item in
            item.isAvailable.toggle()
            return item.update(on: request)
        }
    }
}

