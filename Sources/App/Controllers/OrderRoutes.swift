//
//  OrderRoutes.swift
//  App
//
//  Created by Imthath M on 19/05/19.
//

import Vapor
import FluentPostgreSQL

struct OrderRoutes: RouteCollection {
    
    let paths: PathComponentsRepresentable
    
    public init(_ paths: PathComponentsRepresentable) {
        self.paths = paths
    }
    
    func boot(router: Router) throws {
        let routes = router.grouped(paths)
        
        routes.get(use: list)
        routes.post(OrderInfo.self, use: placeOrder)
        routes.post([ItemDict].self, at: Order.parameter, use:addItems)
        routes.get(Order.parameter, use: checkout)
    }
    
    func placeOrder(_ request: Request, _ orderInfo: OrderInfo) throws -> Future<Order.Full> {
        return try request.parameters.next(Shop.self).flatMap { shop in
            return Order(shopId: shop.id!, placeId: orderInfo.placeId)
                .save(on: request).flatMap { order in
                    self.save(orderInfo.items, orderId: order.id!,
                              shopId: shop.id!, on: request)
                    return try order.totalCost(on: request)
            }
        }
    }

    func addItems(_ request: Request, _ items: [ItemDict]) throws -> Future<Order.Full> {
        return try request.parameters.next(Shop.self).flatMap { shop in
            return try request.parameters.next(Order.self).flatMap { order in
                    self.save(items, orderId: order.id!,
                              shopId: shop.id!, on: request)
                    return try order.totalCost(on: request)
            }
        }
    }
    
    func list(_ request: Request) throws -> Future<[Order]> {
        return try request.parameters.next(Shop.self).flatMap { shop in
            return try shop.orders.query(on: request).all()
        }
    }
    
    func checkout(_ request: Request) throws -> Future<Order.Full> {
        return try request.parameters.next(Shop.self).flatMap { shop in
            return try request.parameters.next(Order.self).flatMap { order in
                return try order.orderItems.query(on: request).all().map { orderItems in
                    return self.fullOrder(of: order, in: shop, for: orderItems.uniqueCount())
                }
            }
        }
    }
    
    private func fullOrder(of order: Order, in shop: Shop, for items: [Item.Info]) -> Order.Full {
        return Order.Full(order, items: items, itemsCost: items.itemsCost,
                          taxAmount: items.tax(for: shop),
                          totalCost: items.totalCost(on: shop))
    }
    
    private func save(_ items: [ItemDict], orderId: Order.ID,
                      shopId: Shop.ID, on conn: DatabaseConnectable) {
        items.forEach { $0.addItem(orderId: orderId, shopId: shopId, on: conn) }
    }
}

struct OrderInfo: Content {
    var placeId: Place.ID
    var items: [ItemDict]
}

struct ItemDict: Content {
    var itemId: UUID
    var count: Int
    
    @discardableResult
    func addItem(orderId: Order.ID, shopId: Shop.ID,
                 on conn: DatabaseConnectable) -> Future<OrderItem>{
        return OrderItem(orderId: orderId, itemId: itemId, shopId: shopId,
                  quantity: count).save(on: conn)
    }
}

