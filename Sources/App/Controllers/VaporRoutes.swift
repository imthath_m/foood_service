//
//  VaporRoutes.swift
//  App
//
//  Created by Imthath M on 17/05/19.
//

import Vapor

final class VaporRoutes<VaporModel: VaporType>: RouteCollection {
    
    let paths: PathComponentsRepresentable
    
    public init(_ paths: PathComponentsRepresentable) {
        self.paths = paths
    }
    
    func boot(router: Router) throws {
        let routes = router.grouped(paths)
        
        routes.get(use: list)
        routes.post(VaporModel.self, use: save)
        routes.put(VaporModel.self, use: update)
    }
    
    func save(_ request: Request, _ model: VaporModel) throws -> Future<VaporModel> {
        return model.save(on: request)
    }
    
    func update(_ request: Request, _ model: VaporModel) throws -> Future<VaporModel> {
        return model.update(on: request)
    }
    
    func list(_ request: Request) throws -> Future<[VaporModel]> {
        return VaporModel.query(on: request).all()
    }
}
