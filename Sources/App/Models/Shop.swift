//
//  Shop.swift
//  App
//
//  Created by Imthath M on 13/05/19.
//

import Vapor
import FluentPostgreSQL

typealias VaporType = Content & Parameter & PostgreSQLUUIDModel & PostgreSQLMigration
typealias FluentType = PostgreSQLUUIDModel & PostgreSQLMigration

final class Shop: VaporType {
    
    var id: UUID?
    var name: String
    var address: String
    var latitude: Double
    var longitude: Double
    var taxPercentage: Int
    
    var places: Children<Shop, Place> {
        return children(\.shopId)
    }
    
    var categories: Children<Shop, Category> {
        return children(\.shopId)
    }
    
    var items: Children<Shop, Item> {
        return children(\.shopId)
    }
    
    var orders: Children<Shop, Order> {
        return children(\.shopId)
    }
    
//    public init(id: UUID?, name: String, address: String,
//                latitude: Double, longitude: Double, taxPercentage: Int) {
//        self.id = id
//        self.name = name
//        self.address = address
//        self.latitude = latitude
//        self.longitude = longitude
//        self.taxPercentage = taxPercentage
//    }
}

struct Location: Codable {
    var latitude: Double
    var longitude: Double
}

extension Shop {
    
    struct Full: Content {
        var id: UUID?
        var name: String
        var latitude: Double
        var longitude: Double
        var taxPercentage: Int
        var places: [Place]?
        var categories: [Category]?
        var items: [Item]?
        
        public init(shop: Shop, places: [Place]? = nil,
                    categories: [Category]? = nil, items: [Item]? = nil) {
            self.id = shop.id
            self.name = shop.name
            self.latitude = shop.latitude
            self.longitude = shop.longitude
            self.taxPercentage = shop.taxPercentage
            self.places = places
            self.categories = categories
            self.items = items
        }
    }
    
    public func places(on connection: DatabaseConnectable) throws -> Future<Full> {
        return try places.query(on: connection).all().map { places in
            return Full(shop: self, places: places)
        }
    }
    
    public func categories(on connection: DatabaseConnectable) throws -> Future<Full> {
        return try categories.query(on: connection).all().map { categories in
            return Full(shop: self, categories: categories)
        }
    }
    
    public func items(on connection: DatabaseConnectable) throws -> Future<Full> {
        return try items.query(on: connection).all().map { items in
            return Full(shop: self, items: items)
        }
    }
}

//struct ShopAddTax: PostgreSQLMigration {
//    static func prepare(on conn: PostgreSQLConnection) -> EventLoopFuture<Void> {
//        return Database.update(Shop.self, on: conn) { builder in
//            builder.field(for: \.taxPercentage, type: .int, PostgreSQLColumnConstraint.default(18))
//        }
//    }
//
//    static func revert(on conn: PostgreSQLConnection) -> EventLoopFuture<Void> {
//        return conn.future()
//    }
//}
